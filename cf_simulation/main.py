import re
import flask

from ecodomus_session_cls import Ecodomus
from util.gcs_to_gcs import to_bak


def push_data_to_ecodomus(request: flask.Request = None):
    """transform ML function output at project "hk-ops-corp-operationdashboard"
    bucket gs://ecodomus/ecospace-digitaltwin-simulation to Ecodomus JSON API
    format and send that over.

        If there is no file in the folder, the function will terminate without error. ML function will handle internally if function caller want to send data to Ecoodmus.

    API doc see gitlab repo

        Args:
            request (flask.Request, optional): Cloud function request body. Defaults to None.

            pass in JSON body {"dev_mode": true} will send data to Ecodomus dev server instead of PD

        Returns:
            [type]: [description]
    """
    measure_point_pattern = re.compile(r"(.*)\((.*)\)")

    # dev_mode = request.get_json().get("dev_mode", False)  # default to be production
    # print(f"dev_mode is set to {str(dev_mode)}")
    dev_mode = True
    with Ecodomus(dev_mode=dev_mode) as e:
        # r = e.get_existing_measure_points()
        # print(r)
        dfs = (
            e.gcs_parquets_to_df_dict()
        )  # <simulation_name>: <pandas dataframe: simulation output>

        # no files in GCS, exit function
        if not dfs:
            msg = "No files in gs://ecodomus/ecospace-digitaltwin-simulation, exit without sending data to Ecodomus"
            print(msg)
            return msg

        for simulation_name, df in dfs.items():

            # add measure point if not exists:
            measure_points = list(df.columns)
            measure_points.remove("DATE")

            for mp in measure_points:
                m = measure_point_pattern.match(mp)
                measure_point_name, unit_of_measure = m.group(1), m.group(2)
                e.add_measure_point(measure_point_name, unit_of_measure)

            payload = e.df_to_json(simulation_name, df)
            print(payload)
            r = e.send_data_to_ecodomus(payload=payload)

            r.raise_for_status()

    # to_bak()

    return "ML function dataframe output pushed to Ecodomus", 200


if __name__ == "__main__":
    from rich import print

    push_data_to_ecodomus()
