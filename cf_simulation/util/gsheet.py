import gspread
import gspread_dataframe as gd
import pandas as pd
from pyasn1.type.univ import Boolean


from secret_manager import SecretManager


class GSheet:
    @classmethod
    def from_secret_manager(cls, project_id, secret_id, version):

        sa_secret = SecretManager().access_secret(
            project_id=project_id, secret_id=secret_id, version=version
        )

        return cls(sa_secret)

    def __init__(self, service_acct_info) -> None:

        self.gc = gspread.service_account_from_dict(service_acct_info)

    def get_as_sheet(
        self, spreadsheet_url_or_key: str, sheet_name: str
    ) -> gspread.Worksheet:
        """get google sheet as gspread worksheet object,
        usually it is easier to use "get_as_df()"

        Args:
            spreadsheet_url_or_key (str): [description]
            sheet_name (str): [description]

        Returns:
            gspread.Worksheet: [description]
        """
        if spreadsheet_url_or_key.startswith("https://docs.google.com/spreadsheets"):
            gc_open_method = getattr(self.gc, "open_by_url")
        else:
            gc_open_method = getattr(self.gc, "open_by_key")

        return gc_open_method(spreadsheet_url_or_key).worksheet(sheet_name)

    def get_as_df(self, spreadsheet_url_or_key: str, sheet_name: str) -> pd.DataFrame:
        """get google sheet as dataframe, also set cls attribute 'last_ws'
            for easy writing back to the same sheet

        Args:
            spreadsheet_url_or_key (str): [description]
            sheet_name (str): [description]

        Returns:
            pd.DataFrame: [description]
        """
        ws = self.get_as_sheet(spreadsheet_url_or_key, sheet_name)
        self.last_ws = ws
        return pd.DataFrame(ws.get_all_records())

    def to_gsheet(
        self,
        df: pd.DataFrame,
        ws: gspread.Worksheet = None,
        append: Boolean = False,
        **kwargs
    ) -> None:
        """write dataframe to gsheet
        refer https://pythonhosted.org/gspread-dataframe/ for other parameter in write options

        Args:
            df (pd.DataFrame): [description]
            ws (gspread.Worksheet, optional): ws to write to, if None. Defaults to None.
            append (Boolean, optional): [description]. Defaults to False.
        """
        #
        if ws is None:
            try:
                ws = self.last_ws
            except AttributeError:
                print(
                    "There is no last read worksheet, need to specify a target worksheet!"
                )
                raise

        if not append:
            ws.clear()
            row = 1
            include_column_header = True
        else:
            row_count = len(ws.get_all_values())
            row = row_count + 1
            include_column_header = False

        gd.set_with_dataframe(
            worksheet=ws,
            dataframe=df,
            row=row,
            include_column_header=include_column_header,
            **kwargs
        )


if __name__ == "__main__":
    spreadsheet_id = "1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus"
    spreadsheet_id = "https://docs.google.com/spreadsheets/d/1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus/edit#gid=0"
    sheet_name = "data"

    gs = GSheet.from_secret_manager(
        project_id="cn-ops-spdigital", secret_id="composer", version="1"
    )
    df = gs.get_as_df(spreadsheet_id, sheet_name)
    gs.to_gsheet(df, append=True)
