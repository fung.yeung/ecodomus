from datetime import datetime
import logging

logger = logging.getLogger(__name__)
from google.cloud import storage


def to_bak(
    gcs_bucket: str = "ecodomus",
    gcs_folder: str = "ecospace-digitaltwin-simulation",
):
    """bak parquets sent to Ecodomus with timestamp info in filename, then
    remove parquet file in staging folder

        Args:
            gcs_bucket (str, optional): [description]. Defaults to "ecodomus".
            gcs_folder (str, optional): [description]. Defaults to "ecospace-digitaltwin-simulation".
    """

    gcs = storage.Client()
    bucket = gcs.bucket(gcs_bucket)
    blobs = gcs.list_blobs(bucket_or_name=gcs_bucket, prefix=gcs_folder)
    now = datetime.utcnow().strftime("%Y%m%d_%Hh%Mm%Ss")
    blobs = [b for b in blobs if b.name != f"{gcs_folder}/"]
    for b in blobs:
        _, blob_name = b.name.split("/")
        blob_folder = "bak"

        fn, ext = blob_name.split(".")
        new_name = f"{blob_folder}/{fn}_{now}.{ext}"
        # print(new_name)
        bucket.copy_blob(blob=b, destination_bucket=bucket, new_name=new_name)
        logger.info(f"copied blob {b.name} to {new_name}")

        # delete "staging" folder blobs
        bucket.delete_blob(b.name)
        logger.info(f"deleted blobs in {gcs_bucket}/{gcs_folder}")


if __name__ == "__main__":
    # to_bak()
    # gcs_bucket: str = "ecodomus"
    # gcs_folder: str = "ecospace-digitaltwin-simulation"
    # gcs = storage.Client()
    # bucket = gcs.bucket(gcs_bucket)
    # blobs = gcs.list_blobs(bucket_or_name=gcs_bucket, prefix=gcs_folder)

    # if list(blobs) == 1:
    pass
