import json
import functions_framework

from util.dcs_cls import DCS


@functions_framework.http
def main(request, dev_mode=None):

    if dev_mode:
        json_body: dict = {
            "measure_time": "2022-10-26T07:12:59+00:00",
            "measure_point": "T-205B (level m3)",
            "measure_value": 0.0,
            "insertion_time": "2022-10-26T07:13:08",
        }
        print("in dev mode")
    else:
        json_body: dict = request.get_json()

    print(json_body)

    # print(context)
    print(json_body)
    dcs = DCS(dev_mode=dev_mode)
    dcs.get_measure_points_id_mapping()

    # add measure_point only when it is not exist in Ecodomus
    try:
        payload = dcs.df_add_dcs_ecodomus_format(json_body)
    except KeyError:
        dcs.add_measure_point(measure_point_name=json_body["measure_point"])
        payload = dcs.df_add_dcs_ecodomus_format(json_body)

    print(payload)
    # r = dcs.send_data_to_ecodomus(payload=payload)
    # print(r.status_code)
    # print(r.text)

    return "done", 200


if __name__ == "__main__":
    main(dev_mode=True)
