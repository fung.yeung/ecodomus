from typing import Set, Union
from urllib.parse import urljoin

# import pandas as pd
import requests

# from google.cloud import storage


class EcodomusAPI(requests.Session):
    """

    Args:
        requests ([type]): [description]
        args, kwargs: requests.Session parameters

    """

    def __init__(self, dev_mode=False, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if dev_mode:
            # self.PREFIX_URL = "https://devservices.ecodomus.com/api/"
            self.PREFIX_URL = "https://devapi.hkves.com"

            # self.PROJECT_ID = "16e0e722-7a92-449e-9282-a93e051a8f2b"
            self.PROJECT_ID = "d095c4de-739e-4074-8163-5c835de63626"  # dev project in ecospace 2022-10-06
            self.dev_mode = True
        else:
            # self.PREFIX_URL = "https://apacservices.ecodomus.com"
            # self.PREFIX_URL = "https://app.hkves.com/ecodomus-services/"
            self.PREFIX_URL = "https://api.hkves.com/"
            self.PROJECT_ID = "f4f7cadd-dcee-4da9-90c2-45604f85097a"
            self.dev_mode = False

        self.CLIENT_ID = "Veolia"
        self.CLIENT_SECRET = "850f9d6c-7f14-4eb9-b241-083b388bff29"
        self.ECODOMUS_USER_NAME = "spasia"
        self.ECODOMUS_PASSWORD = "E*Dtx3dWQ"
        self.access_token = self.get_access_token()

        self.simulations_id_mapping = {
            # dict of simulation name map to simulation id in ecodomus
            "fueling": "fe84e679-7e8f-4959-895b-34fa48c9bcc8",
            "fouling": "0a564cf1-aa64-498d-9b6e-fcecc5f9889c",
            "cleaning": "d8600184-aee1-48e1-ac2d-b0a84f0e375e",
        }

    def get_access_token(self) -> str:
        """get API access token using ecodomous account

        Returns:
            [str]: access token to be attached to header
        """
        url = urljoin(self.PREFIX_URL, "token")

        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        data = {
            "client_id": self.CLIENT_ID,
            "client_secret": self.CLIENT_SECRET,
            "grant_type": "password",
            "username": self.ECODOMUS_USER_NAME,
            "PASSWORD": self.ECODOMUS_PASSWORD,
        }
        r = requests.post(url, headers=headers, data=data)
        return r.json()["access_token"]

    def request(self, method: str, url: str, *args, **kwargs):
        """override request method to add authorization header

        Args:
            method ([]): [description]
            url ([type]): [description]

        Returns:
            [type]: [description]
        """
        # TODO: cater for addition headers
        # to access ecodomus dev server, client_name has to be "EcoDomus", even
        # though getting the access token still need to be using "Veolia" client_id
        # client_name = "EcoDomus" if self.dev_mode else self.CLIENT_ID
        client_name = self.CLIENT_ID

        headers = {
            "authorization": f"bearer {self.access_token}",
            "clientname": client_name,
            "projectId": self.PROJECT_ID,
        }
        url = urljoin(self.PREFIX_URL, url)
        # print(url)
        # print(headers)
        return super().request(method, url, headers=headers, *args, **kwargs)

    def get_existing_measure_points(self, raw=False) -> Union[Set["str"], dict]:
        """return existing measure points already added in Ecodomus as a Set

        Returns:
            [set]: for checking existenance of mp
        """
        r = self.get("/variables")
        self.ecodomus_measure_points = {k["Name"] for k in r.json()}
        if raw:
            return r.json()
        else:
            return self.ecodomus_measure_points

    def get_measure_points_id_mapping(self):
        """return dict with <key>:measure_point_name and <value>: ecodomus mp
        id. To be use by Ecodomus API "PUT /variables/batch"

            Returns:
                [type]: [description]
        """
        resp = self.get_existing_measure_points(raw=True)
        self.measure_points_id_mapping = {mp["Name"]: mp["Id"] for mp in resp}
        return self.measure_points_id_mapping

    def add_measure_point(
        self,
        measure_point_name: str,
        unit_of_measure: str = None,
        measure_point_type: str = "DOUBLE",
        comment: str = "",
        verbose=True,
    ) -> Union[requests.Response, None]:
        """add measure points to Ecodomus, existing mesure points will be
        skipped. Default behaviour from Ecodomus is to create a new measure point with
        numerical increment suffix. Which will make the backend a mess.


        Args:
            measure_point_name (str): [description]
            unit_of_measure (str): [description]
            measure_point_type (str, optional): [description]. Defaults to "DOUBLE".
            comment (str, optional): [description]. Defaults to "".

        Returns:
            Union[requests.Response, None]: [description]
        """

        if getattr(self, "ecodomus_measure_points", None) is None:
            self.get_existing_measure_points()

        measure_point_name = measure_point_name.strip()
        if measure_point_name in self.ecodomus_measure_points:
            r = None
            if verbose:
                print(f"{measure_point_name} already in ecodomus")
        else:
            payload = {
                "Name": measure_point_name,
                "Key": measure_point_name,
                "Type": measure_point_type,
                "Comment": comment,
                "UnitOfMeasure": unit_of_measure,
            }

            r = self.post("/variables", data=payload)
            if verbose:
                print(f"{measure_point_name} created")

        return r

    # def gcs_parquets_to_df_dict(
    #     self,
    #     gcs_bucket: str = "ecodomus",
    #     gcs_folder: str = "ecospace-digitaltwin-simulation",
    # ) -> Dict[str, pd.DataFrame]:
    #     """[summary]

    #     Args:
    #         gcs_bucket (str, optional): [description]. Defaults to "ecodomus".
    #         gcs_folder (str, optional): [description]. Defaults to "ecospace-digitaltwin-simulation".

    #     Raises:
    #         ValueError: [description]

    #     Returns:
    #         Dict[str, pd.DataFrame]: key: simulation name for mapping to
    #         simulation id, val is pandas df turn into Ecodomus API json format
    #     """
    #     output = {}
    #     gcs = storage.Client()
    #     blobs = gcs.list_blobs(bucket_or_name=gcs_bucket, prefix=gcs_folder)
    #     blobs = [b for b in blobs if b.name != f"{gcs_folder}/"]
    #     for b in blobs:
    #         m = re.match(r"^.*\/(\w*)_output.parquet$", b.name)

    #         if m is None and self.dev_mode:  # don't fail the run if dev_mode
    #             print(
    #                 f"gcs parquet name not in standard format, cannot extract simulation type for {b.name}"
    #             )
    #         elif m is None:
    #             raise ValueError(
    #                 "gcs parquet name not in standard format, cannot extract simulation type"
    #             )
    #         else:
    #             simulation_type = m.group(1)
    #             full_gcs_path = f"gs://{gcs_bucket}/{b.name}"
    #             df = pd.read_parquet(full_gcs_path)

    #             output[simulation_type] = df

    #     return output


if __name__ == "__main__":
    # [{'ExecutionTimestamp': '2021-11-03T12:45:12Z',
    #   'Id': 'fe84e679-7e8f-4959-895b-34fa48c9bcc8',
    #   'Name': 'Fuel simulation',
    #   'SimulationId': 'fe84e679-7e8f-4959-895b-34fa48c9bcc8'},
    #  {'ExecutionTimestamp': '2021-11-03T11:45:12Z',
    #   'Id': '0a564cf1-aa64-498d-9b6e-fcecc5f9889c',
    #   'Name': 'Fouling simulation',
    #   'SimulationId': '0a564cf1-aa64-498d-9b6e-fcecc5f9889c'}]
    from rich import print
    from util.helper import get_gsheet_measure_points

    e = EcodomusAPI(dev_mode=True)

    # measure_points = get_gsheet_measure_points()
    with e:
        r = e.get_existing_measure_points()
        print(e.get_measure_points_id_mapping())
        # print(r)
        pass
        # for mp in r:
        #     print(mp)
        # r = e.get_measure_points_id_mapping()
        # print(e.measure_points_id_mapping)
        # for mp in measure_points:
        #     e.add_measure_point(measure_point_name=mp)

        # e.get_measure_points_id_mapping()

        # e.df_to_json
        # print(
        #     e.add_measure_points(
        #         measure_point_name="Fuel optimisation Boiler outlet temperature",
        #         measure_point_type="DOUBLE",
        #         unit_of_measure="degree",
        #     )
        # )
        # e.gcs_parquets_to_df_dict()
        # e.headers["sort"] = '{"ExecutionTimestamp":"Ascending"}'
        # print(e.headers)
        # # r = e.get("/variables")
        # print(r.headers)
        # print(r.status_code)
        # print(r.json())
        # # r = e.get("/floors").json()
        # print(r)
