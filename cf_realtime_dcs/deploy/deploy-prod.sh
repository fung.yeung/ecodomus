gcloud run deploy ecospace-to-ecodomus \
                  --source . \
                  --region asia-northeast1 \
                  --no-allow-unauthenticated \
                  --service-account ecospace-pubsub-runner@cn-ops-spdigital.iam.gserviceaccount.com \
                  --labels project=ecospace,desc=pubsub-to-ecodomus \
                  --cpu 0.5 \
                  --memory 512MiB \
                  --max-instances 2 \
                  --project cn-ops-spdigital