gcloud run deploy ecospace-to-ecodomus \
                  --source . \
                  --region asia-northeast1 \
                  --no-allow-unauthenticated \
                  --service-account ecospace-pubsub-runner@cn-ops-spdigital-dev-dev.iam.gserviceaccount.com \
                  --labels project=ecospace,desc=pubsub-to-ecodomus \
                  --cpu 1 \
                  --memory 512M \
                  --max-instances 2 \
                  --project cn-ops-spdigital-dev-dev