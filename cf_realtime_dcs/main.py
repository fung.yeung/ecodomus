import json
import base64

from flask import Flask, request

from util.dcs_cls import DCS

app = Flask(__name__)


@app.route("/", methods=["POST"])
def index():
    """write pubsub message into Bigquery table using streaming insert

    Returns:
        tuple: HTTP code 204
    """

    # unpack pubsub message
    envelope = request.get_json()
    pubsub_message = envelope["message"]
    data = base64.b64decode(pubsub_message["data"]).decode("utf-8").strip()
    json_body = json.loads(data)

    # print(context)
    # print(json_body)
    dcs = DCS(dev_mode=True)
    dcs.get_measure_points_id_mapping()

    # add measure_point only when it is not exist in Ecodomus
    try:
        payload = dcs.df_add_dcs_ecodomus_format(json_body)
    except KeyError:
        dcs.add_measure_point(measure_point_name=json_body["measure_point"])
        payload = dcs.df_add_dcs_ecodomus_format(json_body)

    r = dcs.send_data_to_ecodomus(payload=payload)
    r.raise_for_status()
    print(f"data sent - {payload}")
    return "done", 200


if __name__ == "__main__":
    from rich import print

    app.run(debug=True)
