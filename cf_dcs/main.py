from datetime import datetime, timedelta
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

import flask
import pandas as pd

from dcs_cls import DCS
from util.bigquery import query_template
from util.helper import get_gsheet_measure_points


def push_dcs_data_to_ecodomus(request: flask.Request = None, dev_mode=None):
    """push DCS data from BigQuery to Ecodomus,
        Request body parameters:
        1. dev_mode(bool) (deprecated): whether to send data to ecoodmus dev
        server, no longer available after UAT
        2. measure_time_lower_bound(str): measure_time lower bound to extract
           data from BigQuery, Defaults to 24 hours ago
        3. measure_time_upper_bound(str): Defaults to now

        Ecodomus API entry point have a time out of couple minutes. So to send
        multiple days data over it is best to seperate the requests by spanning
        between multiple days in easure_time.

    Returns:
        [type]: [description]
    """
    now = datetime.utcnow()
    yesterday = now - timedelta(days=1)

    if request:  # on cloud
        request_json = request.get_json()

        dev_mode = request_json.get(
            "dev_mode", False
        )  # default to be dev, switch back to default to production when ready to launch

        measure_time_lower_bound = request_json.get(
            "measure_time_lower_bound", yesterday
        )
        measure_time_upper_bound = request_json.get("measure_time_upper_bound", now)
    else:  # local
        measure_time_lower_bound = yesterday
        measure_time_upper_bound = now

    logger.info(f"dev_mode is set to {str(dev_mode)}")

    with DCS(dev_mode=dev_mode) as dcs:
        # add measure points to Ecodomus if not exists
        measure_points = get_gsheet_measure_points(push_data_type="dcs")
        for mp in measure_points:
            dcs.add_measure_point(measure_point_name=mp, verbose=False)

        # fetch DCS measure_point to SQL ready format
        measure_points_sql = [f"'{mp}'" for mp in measure_points]
        measure_points_sql = ", ".join(measure_points_sql)

        # return records from BQ base on measure_time and DCS measure poitns, currently
        # hardcoded to fetch all records within 48 hours
        df = pd.read_gbq(
            query_template.format(
                n_hours_ago=24,  # deprecated
                measure_points_list=measure_points_sql,
                measure_time_lower_bound=measure_time_lower_bound,
                measure_time_upper_bound=measure_time_upper_bound,
            )
        )

        # error handling
        # case #1: no record for all measure points
        if df.empty:
            raise ValueError("BigQuery return empty result")
        # convert dataframe to desire output
        dcs.get_measure_points_id_mapping()

        df["output"] = df.apply(dcs.df_add_dcs_ecodomus_format, axis=1)
        payload = list(df["output"])

        logger.info("sending data to ecodomus")
        # send over with Ecoodomus API, takes some time
        r = dcs.send_data_to_ecodomus(payload=payload)
        print(r.status_code)
        print(r.text)

    return "DCS data pushed to Ecodomus", 200


if __name__ == "__main__":
    from rich import print

    # push_dcs_data_to_ecodomus(dev_mode=False)
    with DCS(dev_mode=True) as dcs:
        r = dcs.get_measure_points_id_mapping()
    query = """
SELECT
  DATETIME(measure_time, 'Asia/Hong_Kong') AS measure_time
  , measure_point
  , measure_value
FROM
  `hk-ops-corp-operationdashboard.projects_data.logsheet_ecospace`
WHERE
  1=1
  AND site = 'Ecospace'
  AND measure_point IN ('Cadmium (concentration mg/m3)')
ORDER BY 
  measure_point
  , measure_time    
    """
    df = pd.read_gbq(query)
    df["output"] = df.apply(dcs.df_add_dcs_ecodomus_format, axis=1)
    payload = list(df["output"])
    r = dcs.send_data_to_ecodomus(payload=payload)
    print(r.status_code)
    print(r.text)

    #     print(dcs.get_existing_measure_points())
