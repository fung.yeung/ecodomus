query_template = """
--DECLARE n_hours_ago INT64 DEFAULT {n_hours_ago};
--DECLARE _now TIMESTAMP DEFAULT CURRENT_TIMESTAMP();
--DECLARE _ts_n_hours_ago DEFAULT TIMESTAMP_SUB(_now, INTERVAL n_hours_ago HOUR);
SELECT
  DATETIME(measure_time, 'Asia/Hong_Kong') AS measure_time
  , measure_point
  , measure_value
FROM
  `gbl-imt-ve-datalake-prod.00035_hubgrade_asia.data_asia_raw`
WHERE
  1=1
  AND site = 'Ecospace'
  AND measure_point IN ({measure_points_list})
  -- AND measure_time >= _ts_n_hours_ago
  AND measure_time BETWEEN '{measure_time_lower_bound}' AND '{measure_time_upper_bound}'
ORDER BY 
  measure_point
  , measure_time
;
"""


# query_template = """
# /*
#   - control how many byte scan this query will take by filter using "DATE(measure_time)""
#   - control how many record to send return by changing "rn"
# */

# WITH
#   get_last_n_records_add_rn AS (
#     SELECT
#       measure_point,
#       measure_value,
#       measure_time,
#       ROW_NUMBER()
#         OVER(
#             PARTITION BY
#               measure_point
#             ORDER BY
#               insertion_date DESC ) AS rn
#     FROM
#       `gbl-imt-ve-datalake-prod.00035_hubgrade_asia.data_asia_raw`
#     WHERE
#       1=1
#       AND DATE(measure_time) >= DATE_SUB(CURRENT_DATE(), INTERVAL 5 DAY)
#       AND site = 'Ecospace'
#       AND measure_point IN ({})
#   )
#   , get_last_n_records AS (
#     SELECT
#       measure_point,
#       measure_value,
#       measure_time
#     FROM
#       get_last_n_records_add_rn
#     WHERE
#       rn <= 10
#     )
# SELECT
#   DATETIME(measure_time, 'Asia/Hong_Kong') as measure_time,
#   measure_point,
#   measure_value,
# FROM
#   get_last_n_records
# ORDER BY
#   measure_point, measure_time DESC
# """
