# latest version with

import gspread
import gspread_dataframe as gd
import pandas as pd


from util.secret_manager import SecretManager


class GSheet:
    @classmethod
    def from_secret_manager(cls, project_id, secret_id, version):

        sa_secret = SecretManager().access_secret(
            project_id=project_id, secret_id=secret_id, version=version
        )

        return cls(sa_secret)

    def __init__(self, service_acct_info) -> None:

        self.gc = gspread.service_account_from_dict(service_acct_info)

    def get_as_sheet(
        self, spreadsheet_url_or_key: str, sheet_name: str
    ) -> gspread.Worksheet:
        """get google sheet as gspread worksheet object,
        usually it is easier to use "get_as_df()"

        Args:
            spreadsheet_url_or_key (str): [description]
            sheet_name (str): [description]

        Returns:
            gspread.Worksheet: [description]
        """
        if spreadsheet_url_or_key.startswith("https://docs.google.com/spreadsheets"):
            gc_open_method = getattr(self.gc, "open_by_url")
        else:
            gc_open_method = getattr(self.gc, "open_by_key")

        return gc_open_method(spreadsheet_url_or_key).worksheet(sheet_name)

    def get_as_df(self, spreadsheet_url_or_key: str, sheet_name: str) -> pd.DataFrame:
        """get google sheet as dataframe, also set cls attribute 'last_ws'
            for easy writing back to the same sheet

        Args:
            spreadsheet_url_or_key (str): [description]
            sheet_name (str): [description]

        Returns:
            pd.DataFrame: [description]
        """
        ws = self.get_as_sheet(spreadsheet_url_or_key, sheet_name)
        self.last_ws = ws
        return pd.DataFrame(ws.get_all_records())

    def to_gsheet(
        self,
        df: pd.DataFrame,
        ws: gspread.Worksheet = None,
        append: bool = False,
        **kwargs
    ) -> None:
        """write dataframe to gsheet
        refer https://pythonhosted.org/gspread-dataframe/ for other parameter in write options

        Args:
            df (pd.DataFrame): [description]
            ws (gspread.Worksheet, optional): ws to write to, if None. Defaults to None.
            append (Boolean, optional): [description]. Defaults to False.
        """
        #
        if ws is None:
            try:
                ws = self.last_ws
            except AttributeError:
                print(
                    "There is no last read worksheet, need to specify a target worksheet!"
                )
                raise

        if not append:
            ws.clear()
            row = 1
            include_column_header = True
        else:
            row_count = len(ws.get_all_values())
            row = row_count + 1
            include_column_header = False

        gd.set_with_dataframe(
            worksheet=ws,
            dataframe=df,
            row=row,
            include_column_header=include_column_header,
            **kwargs
        )


def to_ecodomus_format(mp):
    """pass in measure point dataframe's dict

    Args:
        mp ([type]): [description]

    Raises:
        ValueError: [description]

    Returns:
        [type]: [description]
    """
    output = []

    def to_hexcode(color: str):
        mapping = {
            "green": "#00FF00",
            "orange": "#FFA500",
            "red": "#FF0000",
        }
        return mapping[color]

    def extractor(input_val: str, color: str):

        min_val, max_val = None, None
        if input_val == "N/A":  # if N/A, return nothing
            return
        elif "or" in input_val.replace(
            " or ", "or"
        ):  # recursively call extractor for multiple range in a cell
            for iv in input_val.split("or"):
                extractor(input_val=iv, color=color)
        elif "-" in input_val:
            min_val, max_val = input_val.split("-")
        elif "<" in input_val:
            min_val = -99999999
            max_val = input_val.replace("<", "")
        elif ">" in input_val:
            min_val = input_val.replace(">", "")
            max_val = 99999999
        else:
            print(input_val)
            print(color)
            raise ValueError("unable to handle")

        color_to_desc = {
            "#00FF00": "Good range",
            "#FFA500": "Moderate range",
            "#FF0000": "Action needed",
        }

        if min_val and max_val is not None:
            output.append(
                {
                    "MinValue": float(min_val),
                    "MaxValue": float(max_val),
                    "Color": color,
                    "Description": color_to_desc[color],
                }
            )

    for color in ("green", "orange", "red"):
        extractor(mp[color], to_hexcode(color=color))
    return output


def get_gsheet_config():
    spreadsheet_id = "https://docs.google.com/spreadsheets/d/1Ibp4ei37F-jsJrLoWg41wwGGYSZs99N1Kvymgu9hr8E/edit#gid=2139585558"
    sheet_name = "results-20210818-165320"

    gs = GSheet.from_secret_manager(
        project_id="cn-ops-spdigital", secret_id="composer", version="1"
    )
    df_raw = gs.get_as_df(spreadsheet_id, sheet_name)

    # filter only DCS and row that need to send to Ecoodmus
    cond1 = df_raw["type_of_data"].str.lower() == "simulation"
    cond2 = df_raw["send to digital twin ?"].str.lower() == "yes"
    return df_raw[cond1 & cond2]


def get_threshold(df):
    # keep only columns required with key text in header
    target_columns_search_text = ["measure_point", "min", "max"]
    target_df_pos = []
    for idx, c in enumerate(df.columns):
        if any(
            target_c.lower() in c.lower() for target_c in target_columns_search_text
        ):
            target_df_pos.append(idx)

    df = df.iloc[:, target_df_pos]


def color_getter(df):
    """Tranform gsheet color definition into ED API format

    Args:
        df ([type]): [description]

    Returns:
        [type]: [description]
    """

    # keep only columns required with key text in header
    target_columns_search_text = ["measure_point", "green", "orange", "red"]
    target_df_pos = []
    for idx, c in enumerate(df.columns):
        if any(target_c in c.lower() for target_c in target_columns_search_text):
            target_df_pos.append(idx)

    df = df.iloc[:, target_df_pos]
    # rename columns for easier mainpulation
    df = df.rename(
        columns={
            df.columns[idx]: c.replace(" ", "_")
            for idx, c in enumerate(target_columns_search_text)
        }
    )

    df_records = df.to_dict("records")
    color_dict = {mp["measure_point"]: to_ecodomus_format(mp) for mp in df_records}

    return color_dict


if __name__ == "__main__":
    # spreadsheet_id = "1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus"
    # spreadsheet_id = "https://docs.google.com/spreadsheets/d/1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus/edit#gid=0"
    # sheet_name = "data"

    # gs = GSheet.from_secret_manager(
    #     project_id="cn-ops-spdigital", secret_id="composer", version="1"
    # )
    # df = gs.get_as_df(spreadsheet_id, sheet_name)
    # gs.to_gsheet(df, append=True)
    # color_getter()
    df = get_gsheet_config()
    # get_threshold(df)
