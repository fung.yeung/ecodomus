from util.gsheet import GSheet
from util.secret_manager import SecretManager


def _get_gsheet_config(
    push_data_type: str, spreadsheet_id: str = None, sheet_name: str = None
):
    """[summary]

    Args:
        spreadsheet_id ([type]): [description]

    Returns:
        [type]: [description]
    """
    if spreadsheet_id is None:
        spreadsheet_id = "https://docs.google.com/spreadsheets/d/1Ibp4ei37F-jsJrLoWg41wwGGYSZs99N1Kvymgu9hr8E/edit#gid=2139585558"
    if sheet_name is None:
        sheet_name = "results-20210818-165320"

    gs = GSheet.from_secret_manager(
        project_id="cn-ops-spdigital", secret_id="composer", version="1"
    )
    df_raw = gs.get_as_df(spreadsheet_id, sheet_name)

    # filter only DCS and row that need to send to Ecoodmus
    cond1 = (
        df_raw["type_of_data"].str.lower() == push_data_type
    )  # TODO: refactor to cater for both DCS and simulation
    cond2 = df_raw["send to digital twin ?"].str.lower() == "yes"
    df = df_raw[cond1 & cond2]
    return df


def _to_ecodomus_format(mp):
    """pass in measure point dataframe's dict

    Args:
        mp ([type]): [description]

    Raises:
        ValueError: [description]

    Returns:
        [type]: [description]
    """
    output = []

    def to_hexcode(color: str):
        mapping = {
            "green": "#00FF00",
            "orange": "#FFA500",
            "red": "#FF0000",
        }
        return mapping[color]

    def extractor(input_val: str, color: str):

        min_val, max_val = None, None
        if input_val == "N/A":  # if N/A, return nothing
            return
        elif "or" in input_val.replace(
            " or ", "or"
        ):  # recursively call extractor for multiple range in a cell
            for iv in input_val.split("or"):
                extractor(input_val=iv, color=color)
        elif "-" in input_val:
            min_val, max_val = input_val.split("-")
        elif "<" in input_val:
            min_val = -99999999
            max_val = input_val.replace("<", "")
        elif ">" in input_val:
            min_val = input_val.replace(">", "")
            max_val = 99999999
        else:
            print(input_val)
            print(color)
            raise ValueError("unable to handle")

        if min_val and max_val is not None:
            output.append(
                {"MinValue": float(min_val), "MaxValue": float(max_val), "Color": color}
            )

    for color in ("green", "orange", "red"):
        extractor(mp[color], to_hexcode(color=color))
    return output


def get_gsheet_measure_points(push_data_type):
    df = _get_gsheet_config(push_data_type)
    return [mp.strip() for mp in df["measure_point"]]


def get_color():
    """exposed method to get logsheet config's color range as Ecodomus format,
    only Simulation is now using it. DCS data doesn't need to include color info.

        Returns:
            [type]: [description]
    """
    df = _get_gsheet_config(push_data_type="dcs")
    # keep only columns required with key text in header
    target_columns_search_text = ["measure_point", "green", "orange", "red"]
    target_df_pos = []
    for idx, c in enumerate(df.columns):
        if any(target_c in c.lower() for target_c in target_columns_search_text):
            target_df_pos.append(idx)

    df = df.iloc[:, target_df_pos]
    # rename columns for easier mainpulation
    df = df.rename(
        columns={
            df.columns[idx]: c.replace(" ", "_")
            for idx, c in enumerate(target_columns_search_text)
        }
    )

    df_records = df.to_dict("records")
    color_dict = {mp["measure_point"]: _to_ecodomus_format(mp) for mp in df_records}

    return color_dict


def to_dcs_ecodomus_format(x):
    mp_ecodomus_id = mapping[x["measure_point"]]
    return {
        "Id": mp_ecodomus_id,
        "NumericValue": x["measure_value"],
        "LastUpdatedOn": x["measure_time"]
        .to_pydatetime()
        .strftime("%Y-%m-%d %H:%M"),  # .dt.strftime('%Y-%m-%d %H:%M')
    }


if __name__ == "__main__":
    from rich import print

    # c = color_getter()
    # print(c)

    # mp = {
    #     "measure_point": "Incinerator Flue Gas flow Hillside",
    #     "green": "<70000.00",
    #     "orange": "70000.00-75000.00",
    #     "red": ">75000.00",
    # }
    # o = color_extractor(mp)

    # print(o)
