import re
from typing import Set, Union, Dict

# from urllib.parse import urljoin

# import pandas as pd
import requests

# from google.cloud import storage
# from util.helper import get_color, get_gsheet_measure_points
from ecodomus_session_cls import EcodomusAPI


class DCS(EcodomusAPI):
    """DCS class that add methods relvant only to DCS, bulk of the interactivity
    with Ecodomus/bigquery/google sheet/ google cloud storage is in parent class EcodomusAPI
    """

    def __init__(self, dev_mode=False, *args, **kwargs):
        super().__init__(dev_mode=dev_mode, *args, **kwargs)

    def df_add_dcs_ecodomus_format(self, x):
        """function to be use with pd.df.apply(), get DCS ecodomus API JSON
        format with info each each dataframe row

                Args:
                    x ([type]): [description]

                Returns:
                    [type]: [description]
        """
        mp_ecodomus_id = self.measure_points_id_mapping[x["measure_point"]]
        return {
            "Id": mp_ecodomus_id,
            "NumericValue": x["measure_value"],
            "LastUpdatedOn": x["measure_time"]
            .to_pydatetime()
            .strftime("%Y-%m-%d %H:%M"),  # .dt.strftime('%Y-%m-%d %H:%M')
        }

    def send_data_to_ecodomus(self, payload: dict) -> requests.Response:
        """send DCS data to ecodomus

            API entry point "/variables/batch" requires header "select" to
            indicate which field to update. See API doc for details

        Args:
            payload (dict): [description]

        Returns:
            requests.Response: [description]
        """
        additional_headers = {"select": '{"NumericValue", "LastUpdatedOn"}'}

        self.headers.update(additional_headers)
        return self.put("/variables/batch", json=payload)


if __name__ == "__main__":
    # [{'ExecutionTimestamp': '2021-11-03T12:45:12Z',
    #   'Id': 'fe84e679-7e8f-4959-895b-34fa48c9bcc8',
    #   'Name': 'Fuel simulation',
    #   'SimulationId': 'fe84e679-7e8f-4959-895b-34fa48c9bcc8'},
    #  {'ExecutionTimestamp': '2021-11-03T11:45:12Z',
    #   'Id': '0a564cf1-aa64-498d-9b6e-fcecc5f9889c',
    #   'Name': 'Fouling simulation',
    #   'SimulationId': '0a564cf1-aa64-498d-9b6e-fcecc5f9889c'}]
    from rich import print

    dcs = EcodomusAPI(dev_mode=False)
    # dcs = DCS()
    # measure_points = get_gsheet_measure_points()
    with dcs:
        r = dcs.get("/variables")
        import json

        d = json.loads(r.text)
        for mp in d:
            if "reactor" in mp["Name"]:
                print(mp)
        # for line in r.text:
        #     print(line)
        # r = e.get_measure_points_id_mapping()
        # print(e.measure_points_id_mapping)
        # for mp in measure_points:
        #     dcs.add_measure_point(measure_point_name=mp)
        # print(dcs.get_measure_points_id_mapping())

        # e.df_to_json
        # print(
        #     e.add_measure_points(
        #         measure_point_name="Fuel optimisation Boiler outlet temperature",
        #         measure_point_type="DOUBLE",
        #         unit_of_measure="degree",
        #     )
        # )
        # e.gcs_parquets_to_df_dict()
        # e.headers["sort"] = '{"ExecutionTimestamp":"Ascending"}'
        # print(e.headers)
        # # r = e.get("/variables")
        # print(r.headers)
        # print(r.status_code)
        # print(r.json())
        # # r = e.get("/floors").json()
        # print(r)
        pass
