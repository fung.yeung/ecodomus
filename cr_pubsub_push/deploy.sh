gcloud run deploy ecospace-pubsub-runner \
                  --source . \
                  --region asia-northeast1 \
                  --no-allow-unauthenticated \
                  --service-account ecospace-pubsub-runner@cn-ops-spdigital-dev-dev.iam.gserviceaccount.com \
                  --labels project=ecospace,desc=pubsub-to-bq \
                  --project cn-ops-spdigital-dev-dev 