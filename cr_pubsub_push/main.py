import base64

from flask import Flask, request
from google.cloud import bigquery
import json
from datetime import datetime

client = bigquery.Client()
app = Flask(__name__)
TABLE_ID = "cn-ops-spdigital-dev-dev.tests.ecosapce_pubsub"


@app.route("/", methods=["POST"])
def index():
    """write pubsub message into Bigquery table using streaming insert

    Returns:
        tuple: HTTP code 204
    """

    # unpack pubsub message
    envelope = request.get_json()
    pubsub_message = envelope["message"]
    data = base64.b64decode(pubsub_message["data"]).decode("utf-8").strip()
    row_to_insert: dict = json.loads(data)

    # add insertion time metadata column
    now_without_ms = datetime.utcnow().replace(microsecond=0).isoformat()
    row_to_insert.update({"insertion_time": now_without_ms})
    print(f"row to insert: {row_to_insert}")

    # insert to BQ
    errors = client.insert_rows_json(table=TABLE_ID, json_rows=[row_to_insert])

    # error handling
    if not errors:
        print(f"new row added")
    else:
        print(f"Encountered errors while inserting rows: {errors}")
        # TODO: add SendGrid send email

    return ("", 204)


if __name__ == "__main__":
    from rich import print

    app.run(debug=True)
