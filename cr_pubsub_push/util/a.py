

# Construct a BigQuery client object.


# TODO(developer): Set table_id to the ID of table to append to.
table_id = "cn-ops-spdigital-dev-dev.tests.ecosapce_pubsub"

rows_to_insert = [
    {
        "measure_time": "2022-08-02T06:24:33+00:00",
        "measure_point": "test-fung",
        "measure_value": 123,
    },
]


errors = client.insert_rows_json(table_id, rows_to_insert)  # Make an API
if not errors:
    print("New rows have been added.")
else:
    print("Encountered errors while inserting rows: {}".format(errors))
